﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class EMPLEADO
    {
        public string ID_EMPLEADO { get; set; }

        public string NOMBRE { get; set; }

        public string APELLIDO { get; set; }

        public string DIRECCION { get; set; }

        public string ESTADO { get; set; }
    }
}