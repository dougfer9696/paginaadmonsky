﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class USUARIO
    {

        public string ID_USUARIO { get; set; }
        public string NOMBRE { get; set; }

        public string CLAVE { get; set; }
        public string ID_EMPLEADO { get; set; }

        public string ID_ROL { get; set; }


        public string ESTADO { get; set; }
    }
}