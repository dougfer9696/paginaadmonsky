﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class PERFIL
    {
        public string ID_PERFIL { get; set; }

        public string ID_ROL { get; set; }

        public string ID_PANTALLA { get; set; }
    }
}