﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class VISITA_PROGRAMADA
    {
        public string ID_VISITA_PROGRAMADA { get; set; }
        public string ID_CLIENTE { get; set; }
        public string FECHA { get; set; }

        public string DESCRIPCION { get; set; }

        public string ID_USUARIO { get; set; }

        public string FECHA_PROGRAMADA { get; set; }

        public string ESTADO { get; set; }

        public string OBSERVACION { get; set; }
    }
}