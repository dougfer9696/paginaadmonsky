﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class CLIENTE
    {
        public string ID_CLIENTE { get; set; }

        public string NOMBRE { get; set; }

        public string DIRECCION { get; set; }

        public string LONGITUD { get; set; }

        public string LATITUD { get; set; }

        public string NIT { get; set; }

        public string TELEFONO { get; set; }
    }
}