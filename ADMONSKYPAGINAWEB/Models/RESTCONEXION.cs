﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace ADMONSKYPAGINAWEB.Models
{
    public class RESTCONEXION
    {
       

        public string Getinfo(string urlanduri)
        {
            var client = new RestSharp.RestClient(urlanduri);
            client.Timeout = 5000;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);


            return response.Content;

        }

       
        public string Postverb(string urlanduri)
        {
            var client = new RestSharp.RestClient(urlanduri);
            client.Timeout = 5000;
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);


            return response.Content;



        }


        public string Putverb(string urlanduri)
        {
            var client = new RestSharp.RestClient(urlanduri);
            client.Timeout = 5000;
            var request = new RestRequest(Method.PUT);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);


            return response.Content;



        }


        public string Deleteverb(string urlanduri)
        {
            var client = new RestSharp.RestClient(urlanduri);
            client.Timeout = 5000;
            var request = new RestRequest(Method.DELETE);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);


            return response.Content;



        }

    }
}