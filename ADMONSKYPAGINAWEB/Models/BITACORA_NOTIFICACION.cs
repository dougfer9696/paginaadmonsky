﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class BITACORA_NOTIFICACION
    {
        public string ID_BITACORA_NOTIFICACION { get; set; }

        public string ID_VISITA_PROGRAMADA { get; set; }

        public string ID_USUARIO { get; set; }

        public string ESTADO { get; set; }
    }
}