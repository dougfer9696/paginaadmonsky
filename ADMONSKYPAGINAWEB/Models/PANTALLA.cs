﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class PANTALLA
    {
        public string ID_PANTALLA { get; set; }

        public string NOMBRE { get; set; }

        public string DESCRIPCION { get; set; }

        public string ESTADO { get; set; }
    }
}