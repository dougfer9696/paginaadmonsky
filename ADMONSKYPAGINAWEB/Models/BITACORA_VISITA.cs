﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADMONSKYPAGINAWEB.Models
{
    public class BITACORA_VISITA
    {
        public string ID_BITACORA_VISITA { get; set; }

        public string ID_VISITA_PROGRAMADA { get; set; }

        public string FECHA_LLEGADA_VISITA { get; set; }

        public string FECHA_FINALIZACION_VISITA { get; set; }

        public string OBSERVACION { get; set; }

        public string ID_USUARIO { get; set; }

        public string LONGITUD { get; set; }

        public string LATITUD { get; set; }
    }
}