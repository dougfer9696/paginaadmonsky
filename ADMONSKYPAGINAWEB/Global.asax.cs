using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ADMONSKYPAGINAWEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        string cadena;
        protected void Application_Start()
        {
            
            
            DashboardConfig.RegisterService(RouteTable.Routes);
            DevExtremeBundleConfig.RegisterBundles(BundleTable.Bundles);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["ROL"] = cadena;
        }

    }
}
