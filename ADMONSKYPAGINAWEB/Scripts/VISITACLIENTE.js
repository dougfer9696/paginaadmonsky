﻿$(document).ready(function () {

    loadpage();

    function loadpage() {

        $("#datepicker").datepicker({
            format: 'DD-MM-YYYY'
        });


         $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/VISITACLIENTE/CargarSelect',
            cache: false,
            data: {},
            success: function (data) {
                
   

                //    var obj = jQuery.parseJSON(data);

                if (data['Estado'] == 1) {
                    
                    var listausuario = data['listausuario'];
                    var listacliente = data['listacliente'];


                    $('#selcliente').empty();
                    $('#selusuario').empty();


                    listacliente.forEach(function (listacliente) {

                        $('#selcliente').append('<option value="' + listacliente.ID_CLIENTE + '">' + listacliente.NOMBRE + '</option>');
                    });


                    listausuario.forEach(function (listausuario) {

                        $('#selusuario').append('<option value="' + listausuario.ID_USUARIO + '">' + listausuario.NOMBRE + '</option>');
                    });

                    $('#selcliente').select();

                    $('#selusuario').select();

                    $('#selcliente').select('refresh');

                    $('#selusuario').select('refresh');



                }

                if (data['Estado'] == -1) {

                    alert(data['Mensaje']);
                }


            },
            error: function (jqXHR, ex) {

                alert(ex.toString());
            }
        });



    }


    $('#btnnuevovisitaprogramada').on('click', function () {
        
        var cliente = $('#selcliente').val();
        var usuario = $('#selusuario').val();
        var descripcion = $('#txtdescripcion').val().trimEnd();
        var fecha = $('#datepicker').val();
        var estado = $('#selestado').val();
        var observacion = $('#txtobservacion').val().trimEnd();

        var FECHAurl = moment(fecha).format('MM-DD-YYYY');
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/VISITACLIENTE/CrearVISITACLIENTE',
            cache: false,
            
            data: { pcliente: cliente, pdescripcion: descripcion, pusuario: usuario, pfechaprogramada: FECHAurl, pestado : estado, pobservacion : observacion},
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }
                

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#btnactualizarvisitacliente').on('click', function () {
        
        var idvisitacliente = $('#idvisita').val().trimEnd();
        var cliente = $('#selcliente').val().trimEnd();
        var usuario = $('#selusuario').val().trimEnd();
        var descripcion = $('#txtdescripcion').val().trimEnd();
        var fecha = $('#datepicker').val();
        var estado = $('#selestado').val();
        var observacion = $('#txtobservacion').val().trimEnd();
        var FECHAurl = moment(fecha).format('MM-DD-YYYY');
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/VISITACLIENTE/ActualizarVISITACLIENTE',
            cache: false,
            data: { pidVISITACLIENTE: idvisitacliente, pcliente: cliente, pdescripcion: descripcion, pusuario: usuario, pfechaprogramada: FECHAurl, pestado : estado, pobservacion : observacion },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

  
});