﻿$(document).ready(function () {

    loadpage();

    function loadpage() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PERFIL/CargarSelect',
            cache: false,
            data: {},
            success: function (data) {
                

            //    var obj = jQuery.parseJSON(data);

                if (data['Estado'] == 1) {
                    
                    var listarol = data['listarol'];
                    var listapantalla = data['listapantalla'];

                    $('#selrol').empty();
                    $('#selpantalla').empty();


                    listarol.forEach(function (listarol) {

                        $('#selrol').append('<option value="' + listarol.ID_ROL + '">' + listarol.NOMBRE + '</option>');
                    });


                    listapantalla.forEach(function (listapantalla) {

                        $('#selpantalla').append('<option value="' + listapantalla.ID_PANTALLA + '">' + listapantalla.NOMBRE +  '</option>');
                    });

                    $('#selrol').select('refresh');


                    $('#selpantalla').select('refresh');


                }

                if (data['Estado'] == -1) {

                    alert(data['Mensaje'].toString());
                }

            },
            error: function (jqXHR, ex) {

                alert(ex.toString());
            }
        });



    }




    $('#btnnuevoperfil').on('click', function () {
        
        var rol = $('#selrol').val();
        var pantalla = $('#selpantalla').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PERFIL/CrearPERFIL',
            cache: false,
            data: { pidrol: rol, ppantalla: pantalla },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });



    $('#btnactualizarperfil').on('click', function () {
        
        var rol = $('#selrol').val();
        var idperfil = $('#idperfil').val();
        var pantalla = $('#selpantalla').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PERFIL/ActualizarPERFIL',
            cache: false,
            data: { idperfil: idperfil, idrol: rol, idpantalla: pantalla },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

    //
});