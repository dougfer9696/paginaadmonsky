﻿$(document).ready(function () { 

    loadpage();

    function loadpage() {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/USUARIO/CargarSelect',
            cache: false,
            data: {},
            success: function (data) {
                

                if (data['Estado'] == 1) {
                    
                    var listarol = data['listarol'];
                    var listaempleado = data['listaempleado'];

                    $('#selrol').empty();
                    $('#selempleado').empty();

                   
                    listarol.forEach(function (listarol) {

                        $('#selrol').append('<option value="' + listarol.ID_ROL + '">' + listarol.NOMBRE + '</option>');
                    });


                    listaempleado.forEach(function (listaempleado) {

                        $('#selempleado').append('<option value="' + listaempleado.ID_EMPLEADO + '">' + listaempleado.NOMBRE + ' ' +listaempleado.APELLIDO + '</option>');
                    });

                    $('#selrol').select('refresh');


                    $('#selempleado').select('refresh');

                   
                }

                if (data['Estado']==-1) {

                    alert(data['Mensaje'].toString());
                }

            },
            error: function (jqXHR, ex) {

                alert(ex.toString());
            }
        });

   
        
    }


     

    $('#btnnuevousuario').on('click', function () {
        
        var usuario = $('#txtusuario').val();
        var clave = $('#txtclave').val();
        var idempleado = $('#selempleado').val();
        var idrol = $('#selrol').val();
        var estado = $('#selestado').val();

        
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/USUARIO/CrearUsuario',
            cache: false,
            data: { usuario: usuario, clave: clave, idempleado: idempleado, idrol: idrol, estado: estado },
            success: function (data) {

                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }
                
            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#tbactualizar').on('click', function () {
        
        var idusuario = $('#idusuario').val();
        var usuario = $('#txtusuario').val();
        var clave = $('#txtclave').val();
        var idempleado = $('#selempleado').val();
        var idrol = $('#selrol').val();
        var estado = $('#selestado').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/USUARIO/ActualizarUsuario',
            cache: false,
            data: { idusuario:idusuario,usuario: usuario, clave: clave, idempleado: idempleado, idrol: idrol, estado: estado },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


});