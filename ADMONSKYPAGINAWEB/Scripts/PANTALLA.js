﻿$(document).ready(function () {

    $('#btnnuevapantalla').on('click', function () {
        
        var nombre = $('#txtnombre').val();
        var descripcion = $('#txtdescripcion').val();
        var estado = $('#selestado').val();
       


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PANTALLA/CrearPANTALLA',
            cache: false,
            data: { nombre: nombre, descripcion: descripcion, estado: estado },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }
               
            

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#btnactualizarpantalla').on('click', function () {
        
        var idpantalla = $('#idpantalla').val()
        var nombre = $('#txtnombre').val();
        var direccion = $('#txtdescripcion').val();
        var estado = $('#selestado').val();
 


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PANTALLA/ActualizarPANTALLA',
            cache: false,
            data: { pidPANTALLA: idpantalla, nombre: nombre, descripcion: direccion, estado: estado },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });



});