﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Device.Location;
using GoogleApi.Entities;
using GoogleApi.Exceptions;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class CLIENTEController : Controller
    {
        RESTCONEXION restconexion = new RESTCONEXION();
      
        // GET: CLIENTE

        [HttpGet]
        public ActionResult Index()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/CLIENTE";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<CLIENTE>>(json);


            return View(OBJ);
        }



        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearCLIENTE(string nombre = "", string direccion = "", string nit = "", string telefono = "")
        {
            try
            {
                    



                CLIENTE obj = new CLIENTE();
                obj.NOMBRE = nombre.ToString().TrimEnd(); 
                obj.DIRECCION = direccion.ToString().TrimEnd();
                obj.NIT = nit.ToString().TrimEnd();
                obj.TELEFONO = telefono.ToString().TrimEnd();
                obj.LATITUD = "";
                 obj.LONGITUD = "";

                var enviarobj = JsonConvert.SerializeObject(obj);
           
                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostCLIENTE/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();
               

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                return Json(new { Estado = -1, Mensaje = ex.ToString()}, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {


            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetCLIENTEID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var CLIENTE = JsonConvert.DeserializeObject<List<CLIENTE>>(json);
            ViewBag.CLIENTE = CLIENTE;
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(CLIENTE);


        }


        [HttpGet]
        public ActionResult ActualizarCLIENTE(string idcliente="",string nombre = "", string direccion = "", string nit = "", string telefono = "")
        {
            try
            {
                CLIENTE obj = new CLIENTE();
                obj.ID_CLIENTE = idcliente.ToString().TrimEnd();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.DIRECCION = direccion.ToString().TrimEnd();
                obj.NIT = nit.ToString().TrimEnd();
                obj.TELEFONO = telefono.ToString().TrimEnd();
                obj.LATITUD = "";
                obj.LONGITUD = "";
                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutCLIENTE/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetCLIENTEID/'" + id + "'";
            var json = restconexion.Getinfo(url).ToString();

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            var CLIENTE = JsonConvert.DeserializeObject<List<CLIENTE>>(json);
            ViewBag.CLIENTE = CLIENTE;
            return View(CLIENTE);
        }


        [HttpGet]
        public ActionResult DeleteCLIENTE(string idcliente)
        {
            try
            {
                CLIENTE obj = new CLIENTE();
                obj.ID_CLIENTE = idcliente.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/DeleteCLIENTE/" + enviarobj + "";
                var json = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }


        [HttpGet]
        public ActionResult REPCLIENTES()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();

        }

        [HttpGet]
        public ActionResult REPCLIENTEDATOS()
        {
            try
            {
                string url = "http://fernando9696-001-site1.gtempurl.com/api/CLIENTE";
                var json = restconexion.Getinfo(url).ToString();
                var LISTACLIENTES = JsonConvert.DeserializeObject<List<CLIENTE>>(json);
                ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
                return Json(new { data=LISTACLIENTES }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }
        //fin
    }
}