﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class PERFILController : Controller
    {
        RESTCONEXION restconexion = new RESTCONEXION();
        // GET: PERFIL
        public ActionResult Index()
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/PERFIL";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<PERFIL>>(json);

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(OBJ);
        }

        [HttpGet]
        public ActionResult CargarSelect()
        {
            try
            {

                string url = "http://fernando9696-001-site1.gtempurl.com/api/ROL";
                var json = restconexion.Getinfo(url).ToString();

                var lista = new JavaScriptSerializer().Deserialize<List<ROL>>(json);



                string url2 = "http://fernando9696-001-site1.gtempurl.com/api/PANTALLA";
                var json2 = restconexion.Getinfo(url2).ToString();

                var lista2 = new JavaScriptSerializer().Deserialize<List<PANTALLA>>(json2);

                return Json(new { Estado = 1, listarol = lista, listapantalla = lista2 }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                return Json(new { Estado = -1, Mensaje = ex.ToString()}, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearPERFIL(string pidrol = "", string ppantalla = "")
        {
            try
            {
                PERFIL OBJ = new PERFIL();
                OBJ.ID_ROL = pidrol.ToString().TrimEnd();
                OBJ.ID_PANTALLA = ppantalla.ToString().TrimEnd(); 


                var enviarobj = JsonConvert.SerializeObject(OBJ);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostPERFIL/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetPERFILID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var PERFIL = JsonConvert.DeserializeObject<List<PERFIL>>(json);
            ViewBag.PERFIL = PERFIL;
            return View(PERFIL);


        }


        [HttpGet]
        public ActionResult ActualizarPERFIL(string idperfil,string idrol = "", string idpantalla = "")
        {
            try
            {
                PERFIL OBJ = new PERFIL();
                OBJ.ID_PERFIL = idperfil.ToString().TrimEnd(); 
                OBJ.ID_PANTALLA = idpantalla.ToString().TrimEnd();
                OBJ.ID_ROL = idrol.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(OBJ);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutPERFIL/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetPERFILPORID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var PERFIL = JsonConvert.DeserializeObject<List<PERFIL>>(json);

            ViewBag.PERFIL = PERFIL.ToString().TrimEnd();

            PERFIL obj = new PERFIL();
                obj.ID_PERFIL = id.ToString().TrimEnd();

            var enviarobj = JsonConvert.SerializeObject(obj);

                string url2 = "http://fernando9696-001-site1.gtempurl.com/api/DeletePERFIL/" + enviarobj + "";
                var json2 = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json2);

             
            return View(PERFIL);
        }



        //
    }
}