﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class EMPLEADOController : Controller
    {
        // GET: EMPLEADO
        RESTCONEXION restconexion = new RESTCONEXION();
        public ActionResult Index()
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/EMPLEADO";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<EMPLEADO>>(json);

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(OBJ);
        }



      [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }

      

        [HttpGet]
        public ActionResult CrearEmpleado(string nombre = "", string apellido = "", string direccion = "", string estado = "")
        {
            try
            {
                EMPLEADO obj = new EMPLEADO();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.APELLIDO = apellido.ToString().TrimEnd(); 
                obj.DIRECCION = direccion.ToString().TrimEnd();
                obj.ESTADO = estado.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostEMPLEADO/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetEMPLEADOID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var EMPLEADO = JsonConvert.DeserializeObject<List<EMPLEADO>>(json);
            ViewBag.EMPLEADO = EMPLEADO;
            return View(EMPLEADO);


        }


        [HttpGet]
        public ActionResult ActualizarEMPLEADO(string pidempleado, string nombre = "", string apellido = "", string direccion = "", string estado = "")
        {
            try
            {
                EMPLEADO obj = new EMPLEADO();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.APELLIDO = apellido.ToString().TrimEnd();
                obj.DIRECCION = direccion.ToString().TrimEnd();
                obj.ESTADO = estado.ToString().TrimEnd();
                obj.ID_EMPLEADO = pidempleado.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutEMPLEADO/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetEMPLEADOID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var EMPLEADO = JsonConvert.DeserializeObject<List<EMPLEADO>>(json);
            ViewBag.EMPLEADO = EMPLEADO;
            return View(EMPLEADO);
        }


        [HttpGet]
        public ActionResult DeleteEMPLEADO(string idEMPLEADO)
        {
            try
            {
                EMPLEADO obj = new EMPLEADO();
                obj.ID_EMPLEADO = idEMPLEADO.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/DeleteEMPLEADO/" + enviarobj + "";
                var json = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }


    }
}