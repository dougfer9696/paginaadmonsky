﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class VISITACLIENTEController : Controller
    {
        RESTCONEXION restconexion = new RESTCONEXION();
        // GET: VISITACLIENTE
        public ActionResult Index()
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/VISITAPROGRAMADA";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<VISITA_PROGRAMADA>>(json);

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(OBJ);
        }


        [HttpGet]
        public ActionResult CargarSelect()
        {
            try
            {

                string url = "http://fernando9696-001-site1.gtempurl.com/api/USUARIO";
                var json = restconexion.Getinfo(url).ToString();


                var lista = new JavaScriptSerializer().Deserialize<List<USUARIO>>(json);



                string url2 = "http://fernando9696-001-site1.gtempurl.com/api/CLIENTE";
                var json2 = restconexion.Getinfo(url2).ToString();

                var lista2 = new JavaScriptSerializer().Deserialize<List<CLIENTE>>(json2);

                return Json(new { Estado = 1, listausuario = lista, listacliente = lista2 }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearVISITACLIENTE(string pcliente = "", string pdescripcion = "", string pusuario = "", string pfechaprogramada = "",
             string pestado = "", string pobservacion = "")
        {
            try
            {
                VISITA_PROGRAMADA obj = new VISITA_PROGRAMADA();
                obj.ID_CLIENTE = pcliente.ToString().TrimEnd();
                obj.DESCRIPCION = pdescripcion.ToString().TrimEnd();
                obj.ID_USUARIO = pusuario.ToString().TrimEnd();
                obj.FECHA_PROGRAMADA = pfechaprogramada.ToString().TrimEnd();
                obj.ESTADO = pestado.ToString().TrimEnd();
                obj.OBSERVACION = pobservacion.ToString().TrimEnd();
                obj.ID_VISITA_PROGRAMADA = "";
                obj.FECHA = "";

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostVISITA_PROGRAMADA/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

         
                    return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);
           
              

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetVISITA_PROGRAMADAID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var VISITACLIENTE = JsonConvert.DeserializeObject<List<VISITA_PROGRAMADA>>(json);
            ViewBag.VISITACLIENTE = VISITACLIENTE;
            return View(VISITACLIENTE);


        }


        [HttpGet]
        public ActionResult ActualizarVISITACLIENTE(string pidVISITACLIENTE = "", string pcliente = "", string pdescripcion = "", string pusuario = "",
            string pfechaprogramada = "",string pestado = "", string pobservacion = "")
        {
            try
            {
                VISITA_PROGRAMADA obj = new VISITA_PROGRAMADA();
                obj.ID_VISITA_PROGRAMADA = pidVISITACLIENTE.ToString().TrimEnd(); 
                obj.ID_CLIENTE = pcliente.ToString().TrimEnd(); 
                obj.DESCRIPCION = pdescripcion.ToString().TrimEnd();
                obj.ID_USUARIO = pusuario.ToString().TrimEnd();
                obj.FECHA_PROGRAMADA = pfechaprogramada.ToString().TrimEnd();
                obj.ESTADO = pestado.ToString().TrimEnd();
                obj.OBSERVACION = pobservacion.ToString().TrimEnd();
                obj.FECHA = "";
                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutVISITA_PROGRAMADA/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetVISITA_PROGRAMADAID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var VISITACLIENTE = JsonConvert.DeserializeObject<List<VISITA_PROGRAMADA>>(json);
            ViewBag.VISITACLIENTE = VISITACLIENTE;
            return View(VISITACLIENTE);
        }


        [HttpGet]
        public ActionResult DeleteVISITACLIENTE(string idVISITACLIENTE)
        {
            try
            {
                VISITA_PROGRAMADA obj = new VISITA_PROGRAMADA();
                obj.ID_VISITA_PROGRAMADA = idVISITACLIENTE;
              
                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/DeleteVISITA_PROGRAMADA/" + enviarobj + "";
                var json = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public ActionResult REPVISITACLIENTE()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }


     
        //
    }
}