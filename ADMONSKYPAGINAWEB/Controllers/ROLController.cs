﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADMONSKYPAGINAWEB.Controllers
{
   
    public class ROLController : Controller
    {
        // GET: ROL
        RESTCONEXION restconexion = new RESTCONEXION();
        public ActionResult Index()
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/ROL";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<ROL>>(json);

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(OBJ);
        }


        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearROL(string nombre = "", string descripcion = "")
        {
            try
            {
                ROL obj = new ROL();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.DESCRIPCION = descripcion.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostROL/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetROLID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var ROL = JsonConvert.DeserializeObject<List<ROL>>(json);
            ViewBag.ROL = ROL;
            return View(ROL);


        }


        [HttpGet]
        public ActionResult ActualizarROL(string pidROL, string nombre = "", string descripcion = "")
        {
            try
            {
                ROL obj = new ROL();
                obj.NOMBRE = nombre.ToString().TrimEnd(); ;
                obj.DESCRIPCION = descripcion.ToString().TrimEnd(); ;
                obj.ID_ROL = pidROL.ToString().TrimEnd(); ;

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutROL/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetROLID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var ROL = JsonConvert.DeserializeObject<List<ROL>>(json);
            ViewBag.ROL = ROL;
            return View(ROL);
        }


        [HttpGet]
        public ActionResult DeleteROL(string idROL)
        {
            try
            {
                ROL obj = new ROL();
                obj.ID_ROL = idROL.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/DeleteROL/" + enviarobj + "";
                var json = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }


        //
    }
}