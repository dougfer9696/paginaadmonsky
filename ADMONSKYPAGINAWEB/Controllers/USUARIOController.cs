﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class USUARIOController : Controller
    {
        RESTCONEXION restconexion = new RESTCONEXION();
     
        [HttpGet]
        public ActionResult CargarSelect()
        {
            try
            {

                string url = "http://fernando9696-001-site1.gtempurl.com/api/ROL";
                var json = restconexion.Getinfo(url).ToString();


                var lista = new JavaScriptSerializer().Deserialize<List<ROL>>(json);



                string url2 = "http://fernando9696-001-site1.gtempurl.com/api/EMPLEADO";
                var json2 = restconexion.Getinfo(url2).ToString();

                var lista2 = new JavaScriptSerializer().Deserialize<List<EMPLEADO>>(json2);

                return Json(new { Estado = 1, listarol = lista, listaempleado = lista2 }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpGet]
        public ActionResult Cargarusuarios()
        {
            try
            {

                string url = "http://fernando9696-001-site1.gtempurl.com/api/USUARIO/";
                var json = restconexion.Getinfo(url).ToString();


                var lista = new JavaScriptSerializer().Deserialize<List<USUARIO>>(json);


                return Json(new { Estado = 1, listausuario = lista }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult Index()
        {


            string url = "http://fernando9696-001-site1.gtempurl.com/api/USUARIO/";
            var json = restconexion.Getinfo(url).ToString();
            var usuario = JsonConvert.DeserializeObject<List<USUARIO>>(json);
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(usuario);
        }


        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearUsuario(string usuario = "", string clave = "", string idempleado = "", string idrol = "", string estado = "")
        {
            try
            {
                USUARIO user = new USUARIO();
                user.CLAVE = clave;
                user.NOMBRE = usuario;
                user.ESTADO = estado;
                user.ID_EMPLEADO = idempleado;
                user.ID_ROL = idrol;

                var enviarobj = JsonConvert.SerializeObject(user);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostUSUARIO/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetUSUARIOPORID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var usuario = JsonConvert.DeserializeObject<List<USUARIO>>(json);
            ViewBag.USUARIO = usuario;
            return View(usuario);


        }


        [HttpGet]
        public ActionResult ActualizarUsuario(string idusuario="", string usuario = "", string clave = "", string idempleado = "", string idrol = "", string estado = "")
        {
            try {
                USUARIO user = new USUARIO();
                user.CLAVE = clave.TrimEnd();
                user.NOMBRE = usuario.TrimEnd();
                user.ESTADO = estado.TrimEnd();
                user.ID_EMPLEADO = idempleado.TrimEnd();
                user.ID_ROL = idrol.TrimEnd();
                user.ID_USUARIO = idusuario.TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(user);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutUSUARIO/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetUSUARIOPORID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var usuario = JsonConvert.DeserializeObject<List<USUARIO>>(json);
            ViewBag.USUARIO = usuario;
            return View(usuario);
        }


        [HttpGet]
public ActionResult Deleteusuario(string idusuario)
        {

           
            try { 
    USUARIO user = new USUARIO();
    user.ID_USUARIO = idusuario;

    var enviarobj = JsonConvert.SerializeObject(user);

    string url = "http://fernando9696-001-site1.gtempurl.com/api/DeleteUSUARIO/" + enviarobj + "";
    var json = restconexion.Deleteverb(url).ToString();

   RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

    return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

}
            catch(Exception ex)
            {
               
                return Json(new { Estado = -1, Mensaje = ex.ToString() }, JsonRequestBehavior.AllowGet);

}

        }



    }
}
