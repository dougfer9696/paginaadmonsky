﻿using ADMONSKYPAGINAWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class PANTALLAController : Controller
    {
        RESTCONEXION restconexion = new RESTCONEXION();
        // GET: PANTALLA
        public ActionResult Index()
        {
            string url = "http://fernando9696-001-site1.gtempurl.com/api/PANTALLA";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<PANTALLA>>(json);

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View(OBJ);
        }


        [HttpGet]
        public ActionResult Create()
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }



        [HttpGet]
        public ActionResult CrearPANTALLA(string nombre = "", string descripcion = "", string estado = "")
        {
            try
            {
                PANTALLA obj = new PANTALLA();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.DESCRIPCION = descripcion.ToString().TrimEnd();
                obj.ESTADO = estado.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PostPANTALLA/" + enviarobj + "";
                var json = restconexion.Postverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);
                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);


            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {

            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetPANTALLAID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var PANTALLA = JsonConvert.DeserializeObject<List<PANTALLA>>(json);
            ViewBag.PANTALLA = PANTALLA;
            return View(PANTALLA);


        }


        [HttpGet]
        public ActionResult ActualizarPANTALLA(string pidPANTALLA, string nombre = "", string descripcion = "", string estado = "")
        {
            try
            {
                PANTALLA obj = new PANTALLA();
                obj.NOMBRE = nombre.ToString().TrimEnd();
                obj.DESCRIPCION = descripcion.ToString().TrimEnd();
                obj.ESTADO = estado.ToString().TrimEnd();
                obj.ID_PANTALLA = pidPANTALLA.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/PutPANTALLA/" + enviarobj + "";
                var json = restconexion.Putverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }



        [HttpGet]
        public ActionResult Delete(string id)
        {
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetPANTALLAID/" + id + "";
            var json = restconexion.Getinfo(url).ToString();
            var PANTALLA = JsonConvert.DeserializeObject<List<PANTALLA>>(json);
            ViewBag.PANTALLA = PANTALLA;
            return View(PANTALLA);
        }


        [HttpGet]
        public ActionResult DeletePANTALLA(string pidPANTALLA, string estado)
        {
            try
            {

                PANTALLA obj = new PANTALLA();
            
                obj.ESTADO = estado.ToString().TrimEnd();
                obj.ID_PANTALLA = pidPANTALLA.ToString().TrimEnd();

                var enviarobj = JsonConvert.SerializeObject(obj);

                string url = "http://fernando9696-001-site1.gtempurl.com/api/DeletePANTALLA/" + enviarobj + "";
                var json = restconexion.Deleteverb(url).ToString();

                RESPUESTA response = JsonConvert.DeserializeObject<RESPUESTA>(json);

                return Json(new { Estado = response.ESTADO, Mensaje = response.DESCRIPCION }, JsonRequestBehavior.AllowGet);


            }
            catch
            {
                return Json(new { Estado = -1, Mensaje = "Error revise informacion" }, JsonRequestBehavior.AllowGet);

            }

        }


        //
    }
}