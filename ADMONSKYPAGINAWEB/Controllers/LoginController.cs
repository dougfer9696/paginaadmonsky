﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestClient.Net;
using ADMONSKYPAGINAWEB.Models;

namespace ADMONSKYPAGINAWEB.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            
            ViewData["ROL"] = Session["ROL"].ToString().TrimEnd();
            return View();
        }

        public ActionResult ValidarUsuario(string pusuario, string pclave)
        {


            RESTCONEXION restconexion = new  RESTCONEXION();
            
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetUSUARIOPORNOMBRE/" + pusuario.ToString().TrimEnd()+"";
            var json = restconexion.Getinfo(url).ToString();
            var usuario = JsonConvert.DeserializeObject<List<USUARIO>>(json);

            foreach (USUARIO user in usuario)
            {
                if (user.ESTADO.ToString().TrimEnd() == "A")
                {
                    if (user.NOMBRE.ToString().Trim() == pusuario.ToString().TrimEnd())
                    {
                        if (user.CLAVE.ToString().Trim() == pclave.ToString().TrimEnd())
                        {
                          
                            Session["ROL"] = user.ID_ROL.ToString().TrimEnd();
                            return Json(new { Estado = 1, idrol= user.ID_ROL, Mensaje = "Usuario correcto" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Estado = 2, Mensaje = "Contraseña Incorrecta" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                         return Json(new { Estado = 2, Mensaje = "Usuario Incorrecto" },JsonRequestBehavior.AllowGet );
                    }
                }

            }
             return Json(new { Estado = -1, Mensaje = "ERROR EN DATOS" }, JsonRequestBehavior.AllowGet);

            //return View();


        }

        public ActionResult ValidarRol(string rol)
        {


            RESTCONEXION restconexion = new RESTCONEXION();

            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetPERFILIDROL/" + rol.ToString().TrimEnd() + "";
            var json = restconexion.Getinfo(url).ToString();
            var perfiles = JsonConvert.DeserializeObject<List<PERFIL>>(json);

            string url2 = "http://fernando9696-001-site1.gtempurl.com/api/PANTALLA";
            var json2 = restconexion.Getinfo(url2).ToString();
            var pantallas = JsonConvert.DeserializeObject<List<PANTALLA>>(json2);
            List<string> listapantalla = new List<string>();
            List<string> listapantallamenu = new List<string>();
            listapantallamenu.Add("ROL");
            listapantallamenu.Add("PERFIL");
            listapantallamenu.Add("PANTALLA");
            listapantallamenu.Add("EMPLEADO");
            listapantallamenu.Add("USUARIO");
            listapantallamenu.Add("CLIENTE");
            listapantallamenu.Add("REPCLIENTES");
            listapantallamenu.Add("VISITACLIENTES");
            listapantallamenu.Add("DASVISITACLIENTES");
            listapantallamenu.Add("REPVISITASCLIENTES");
      
            foreach (var per in perfiles)
            {
                var idpantalla = per.ID_PANTALLA;
                foreach (var pan in pantallas)
                {
                    if (idpantalla == pan.ID_PANTALLA)
                    {
                        listapantalla.Add(pan.NOMBRE);
                    }
                }
            

            }


            return Json(new { Estado = 1, listapantalla = listapantalla, listapantamenus = listapantallamenu }, JsonRequestBehavior.AllowGet);

            //return View();


        }

    }
}
