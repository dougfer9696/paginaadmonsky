﻿$(document).ready(function () {


    DevExpress.localization.locale(navigator.language);

    function loadpage() {

        var datasourerepcliente = new DevExpress.data.CustomStore({
            load: function (loadOptions) {

                var d = $.Deferred();
                $.ajax({
                    type: 'GET',
                    url: '/CLIENTE/REPCLIENTEDATOS',
                    success: function (data) {
                        data = JSON && JSON.parse(data) || $.parseJSON(data);
                        d.resolve(data);
                    },
                    error: function (jqXHR, ex) {
                        alert(ex.toString());
                    }

                });

                return d.promise();

            }

        });

        var gridinfo = $('#gridcontainerclientes').dxDataGrid({
            dataSource: new DevExpress.data.DataSource(datasourerepcliente),
            showBorders: true,
            columnAutoWidth: true,
            pager: {
                showPageSizeSelector: true,
                allowePageSizes: [10, 20, 50, 100],
                showNavigationButtons: true,
                showInfo: true,
                infoText: "Pagina {0} de {1} ({2} items)"
            },
            loadpanel: {
                text: "Cargando...."
            },
            columns: [
                {
                    datafield: "id_cliente",
                    datatype: "number",
                    caption: "Codigo Cliente",
                    visible: true
                },
                {
                    datafield: "nombre",
                    datatype: "string",
                    caption: "CLIENTE",
                    visible: true
                },
                {
                    datafield: "direccion",
                    datatype: "string",
                    caption: "DIRECCION",
                    visible: true
                },
                {
                    datafield: "longitud",
                    datatype: "string",
                    caption: "Longitud",
                    visible: true
                },
                {
                    datafield: "latitud",
                    datatype: "string",
                    caption: "Latitud",
                    visible: true
                },
                {
                    datafield: "nit",
                    datatype: "string",
                    caption: "NIt",
                    visible: true
                },
                {
                    datafield: "telefono",
                    datatype: "number",
                    caption: "Telefono",
                    visible: true
                },
                
            ],

        });
});