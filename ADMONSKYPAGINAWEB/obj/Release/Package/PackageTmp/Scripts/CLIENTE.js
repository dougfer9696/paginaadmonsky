﻿$(document).ready(function () {


    $('#btnnuevocliente').on('click', function () {
        
        var nombre = $('#txtnombre').val().trimEnd();
        var direccion = $('#txtdireccion').val().trimEnd();
        var nit = $('#txtnit').val().trimEnd();
        var telefono = $('#txttelefono').val().trimEnd();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/CLIENTE/CrearCLIENTE',
            cache: false,
            data: { nombre: nombre, direccion: direccion, nit: nit, telefono: telefono },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#btnactualizarcliente').on('click', function () {
        
        var idcliente = $('#idcliente').val().trimEnd();
        var nombre = $('#txtnombre').val().trimEnd();
        var direccion = $('#txtdireccion').val().trimEnd();
        var nit = $('#txtnit').val().trimEnd();
        var telefono = $('#txttelefono').val().trimEnd();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/CLIENTE/ActualizarCLIENTE',
            cache: false,
            data: { idcliente: idcliente, nombre: nombre, direccion: direccion, nit: nit, telefono: telefono },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });



});