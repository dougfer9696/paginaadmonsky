﻿$(document).ready(function () {




    $('#bteliminarusuario').on('click', function () {
        
        var usuario = $('#idusuario').val();
    
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/USUARIO/Deleteusuario',
            cache: false,
            data: { idusuario: usuario },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

    $('#bteliminarrol').on('click', function () {
        
        var id = $('#idrol').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/ROL/DeleteROL',
            cache: false,
            data: { idROL: id },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

    $('#bteliminarpantalla').on('click', function () {
        
        var id = $('#idpantalla').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/PANTALLA/DeletePANTALLA',
            cache: false,
            data: { pidPANTALLA: id, estado: 'I' },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

    $('#bteliminarempleado').on('click', function () {
        
        var id = $('#idempleado').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/EMPLEADO/DeleteEMPLEADO',
            cache: false,
            data: { idEMPLEADO: id },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });




    $('#bteliminarcliente').on('click', function () {
        
        var cliente = $('#idcliente').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/CLIENTE/DeleteCLIENTE',
            cache: false,
            data: { idcliente: cliente },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });

    



    $('#bteliminarvisita').on('click', function () {
        
        var id = $('#idvisita').val();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/VISITACLIENTE/DeleteVISITACLIENTE',
            cache: false,
            data: { idVISITACLIENTE: id},
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });
});