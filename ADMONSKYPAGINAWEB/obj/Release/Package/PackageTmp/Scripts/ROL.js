﻿$(document).ready(function () {

    $('#btnnuevorol').on('click', function () {
        
        var nombre = $('#txtnombre').val();
        var descripcion = $('#txtdescripcion').val();



        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/ROL/CrearROL',
            cache: false,
            data: { nombre: nombre, descripcion: descripcion },
            success: function (data) {
                
                if (data['Estado'] == 1) {
                    alert(data['Mensaje'] .toString());

                }

                if (data['Estado'] == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#btnactualizarROL').on('click', function () {
        
        var idrol = $('#idrol').val()
        var nombre = $('#txtnombre').val();
        var descripcion = $('#txtdescripcion').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/ROL/ActualizarROL',
            cache: false,
            data: { pidROL: idrol, nombre: nombre, descripcion: descripcion },
            success: function (data) {
                
                if (data['Estado'] == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'] == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });



});