﻿$(document).ready(function () {
    cargarmenu();
    const $dropdown = $(".dropdown");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");



    $dropdown.hover(
        function () {
            const $this = $(this);
            //  $this.addClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "true");
            // $this.find($dropdownMenu).addClass(showClass);
        },
        function () {
            const $this = $(this);
            // $this.removeClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "false");
            $//this.find($dropdownMenu).removeClass(showClass);
        }
    );

    function cargarmenu() {
        
        var idrol = $('#idrol').val().trimEnd();
        if (
            idrol > 0 
        ) {

            idrol = idrol;
        } else {
            idrol = 0;
        }

        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/Login/ValidarRol',
            cache: false,
            data: { rol: idrol },
            success: function (data) {

                var estado = data['Estado'];


                if (estado == 1) {
                    
                    var listapantalla = data['listapantalla'];
                    var listapantallamenu = data['listapantamenus'];

                    for (var i = 0; i < listapantalla.length; i++) {
                        var pantalla = listapantalla[i];
                        for (var j = 0; j < listapantallamenu.length; j++) {

                            var menu = listapantallamenu[j];
                            if (menu == pantalla) {


                                $('#' + menu).removeClass("invisible");
                            }

                        }
                    }





                }
                if (estado == 2) {
                    alert(data['Mensaje']);
                }
                if (estado == -1) {
                    alert(data['Mensaje']);
                }


            },
            error: function (jqXHR, ex) {
                


            }




        });
    }

       
    

    });


