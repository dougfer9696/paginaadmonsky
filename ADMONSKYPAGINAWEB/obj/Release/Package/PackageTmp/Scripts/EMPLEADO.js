﻿$(document).ready(function () {





    $('#btnnuevoempleado').on('click', function () {
        
        var nombre = $('#txtnombre').val();
        var apellido = $('#txtapellido').val();
        var direccion = $('#txtdireccion').val();
        var estado = $('#selestado').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/EMPLEADO/CrearEmpleado',
            cache: false,
            data: { nombre: nombre, apellido: apellido, direccion: direccion, estado: estado },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


    $('#btnactualizarusuario').on('click', function () {
        
        var idempleado = $('#idempleado').val()
        var nombre = $('#txtnombre').val();
        var apellido = $('#txtapellido').val();
        var direccion = $('#txtdireccion').val();
        var estado = $('#selestado').val();


        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: '/EMPLEADO/ActualizarEMPLEADO',
            cache: false,
            data: { pidempleado: idempleado,nombre: nombre, apellido: apellido, direccion: direccion, estado: estado },
            success: function (data) {
                
                if (data['Estado'].toString() == 1) {
                    alert(data['Mensaje'].toString());

                }

                if (data['Estado'].toString() == -1) {
                    alert(data['Mensaje'].toString());

                }

            },
            error: function (jqXHR, ex) {
                alert(ex.toString());
            }
        });

    });


  
});