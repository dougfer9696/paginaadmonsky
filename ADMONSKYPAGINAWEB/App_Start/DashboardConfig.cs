using System;
using System.Collections.Generic;
using System.Web.Routing;
using System.Xml.Linq;
using ADMONSKYPAGINAWEB.Models;
using DevExpress.DashboardCommon;
using DevExpress.DashboardWeb;
using DevExpress.DashboardWeb.Mvc;
using DevExpress.DataAccess.Json;
using Newtonsoft.Json;


namespace ADMONSKYPAGINAWEB
{
    public class DashboardConfig
    {
     

        public static void RegisterService(RouteCollection routes)
        {
            routes.MapDashboardRoute("api/dashboard", "DefaultDashboard");

            // Uncomment this line to save dashboards to the App_Data folder.
            //  DashboardConfigurator.Default.SetDashboardStorage(new DashboardFileStorage(@"~/App_Data/DASHBOARD"));

            // Uncomment these lines to create an in-memory storage of dashboard data sources. Use the DataSourceInMemoryStorage.RegisterDataSource
            // method to register the existing data source in the created storage.
            //var dataSourceStorage = new DataSourceInMemoryStorage();
            //DashboardConfigurator.Default.SetDataSourceStorage(dataSourceStorage);

            //  DashboardFileStorage dashboardFileStorage = new DashboardFileStorage("~/App_Data/Dashboards");
            //  DashboardConfigurator.Default.SetDashboardStorage(dashboardFileStorage);

            // Create a data source storage.






            RESTCONEXION restconexion = new RESTCONEXION();
            string url = "http://fernando9696-001-site1.gtempurl.com/api/GetVISITA_PROGRAMADADASDASH";
            var json = restconexion.Getinfo(url).ToString();
            var OBJ = JsonConvert.DeserializeObject<List<VISITADASH>>(json);

            DataSourceInMemoryStorage dataSourceStorage = new DataSourceInMemoryStorage();

            // Register an Object data source.
            DashboardObjectDataSource objDataSource = new DashboardObjectDataSource("DATA");
            // objDataSource.DataId = "objectDataSource";

            dataSourceStorage.RegisterDataSource("objDataSource", objDataSource.SaveToXml());

            DashboardConfigurator.Default.DataLoading += (s, e) => {
                if (e.DataSourceName == "DATA")
                {
                    e.Data = OBJ;
                }
            };

         
            // Set the configured data source storage.
            DashboardConfigurator.Default.SetDataSourceStorage(dataSourceStorage);

            //guardar dasboard
        
            DashboardConfigurator.Default.SetDashboardStorage(new DashboardFileStorage(@"~/REPORTES/DASHBOARD"));
        }
    }


 

}